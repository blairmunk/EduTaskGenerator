from abc import ABC, abstractmethod

class InterfaceTaskBank(ABC):
    '''
    Interface for task bank
    '''

    @abstractmethod
    def load():
        pass

    @abstractmethod
    def save():
        pass

    @abstractmethod
    def create():
        pass
    
    @abstractmethod
    def delete():
        pass