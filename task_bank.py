from interfaces import InterfaceTaskBank
import os
import json

class Section(InterfaceTaskBank):
    def __init__(self, name, task_groups=[]):
        self.name = name
        self.task_groups = task_groups

    def save(self):
        sections = self.load()
        sections[self.name] = self.task_groups
        with open('task_bank/sections.json', 'w', encoding='utf-8') as f:
            json.dump(sections, f, ensure_ascii=False)  # ensure_ascii=False позволяет записывать не-ASCII символы напрямую)
    
    def create(self):
        pass

    def delete(self):
        pass

    @staticmethod
    def load():
        if os.path.exists('task_bank/sections.json'):
            with open('task_bank/sections.json', 'r') as f:
                sections = json.load(f)
        else:
            sections = {}
        return sections


    

