from flask import Flask, request, render_template
from task_bank import Section

app = Flask(__name__)
app.config['JSON_AS_ASCII'] = False  # Это позволит использовать не-ASCII символы в JSON-ответах

@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        section_name = request.form.get('section_name')
        Section(section_name).save()
    sections = Section.load()
    return render_template('index.html', sections=sections)

if __name__ == '__main__':
    app.run(debug=True)